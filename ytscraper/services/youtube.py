"""
    Main Service implementation.
"""

from typing import Optional, List, Union

import requests
from requests.models import Response

from ytscraper.errors import ErrorCode, ErrorMessage, YouTubeException
from ytscraper.utils.params_checker import enf_comma_separated, enf_parts


class YTScraperService(object):
    """
    Example usage:
        To create an instance of pyyoutube.Api class:
            >>> api = YTScraperService(api_key="your api key")
        To get one channel info:
            >>> res = api.get_channel_info(channel_name="googledevelopers")
            >>> print(res.items[0])
    """

    BASE_URL = "https://www.googleapis.com/youtube/v3/"

    DEFAULT_REDIRECT_URI = "https://localhost/"

    DEFAULT_STATE = "YouTubeScraper"
    DEFAULT_TIMEOUT = 10

    def __init__(
        self,
        api_key: Optional[str] = None,
        timeout: Optional[int] = None,
        proxies: Optional[dict] = None,
    ) -> None:
        """
        This Api provide two method to work. Use api key or use access token.
        Args:
            api_key(str, optional):
                The api key which you create from google api console.
            timeout(int, optional):
                The request timeout.
            proxies(dict, optional):
                If you want use proxy, need point this param.
                param style like requests lib style.
                Refer https://2.python-requests.org//en/latest/user/advanced/#proxies
        Returns:
            YouTube Api instance.
        """
        self._api_key = api_key
        self._refresh_token = None  # This keep current user's refresh token.
        self._timeout = timeout
        self.session = requests.Session()
        self._oauth_session = None
        self.proxies = proxies

        if not (self._api_key):
            raise YouTubeException(
                ErrorMessage(
                    status_code=ErrorCode.MISSING_PARAMS,
                    message="Must specify either client key info or api key.",
                )
            )

        if self._timeout is None:
            self._timeout = self.DEFAULT_TIMEOUT

    @staticmethod
    def _parse_response(response: Response) -> dict:
        """
        Parse response data and check whether errors exists.
        Args:
            response (Response)
                The response which the request return.
        Return:
             response's data
        """
        data = response.json()
        if "error" in data:
            raise YouTubeException(response)
        return data

    @staticmethod
    def _parse_data(data: Optional[dict]) -> Union[dict, list]:
        """
        Parse resp data.
        Args:
            data (dict)
                The response data by response.json()
        Return:
             response's items
        """
        items = data["items"]
        return items

    def _request(self, resource, method=None, args=None) -> Response:
        """
        Main request sender.
        Args:
            resource(str)
                Resource field is which type data you want to retrieve.
                Such as channels，videos and so on.
            method(str, optional)
                The method this request to send request.
                Default is 'GET'
            args(dict, optional)
                The url params for this request.
        Returns:
            response
        """
        if method is None:
            method = "GET"

        if args is None:
            args = dict()

        key = None
        access_token = None
        if self._api_key is not None:
            key = "key"
            access_token = self._api_key

        if access_token is None:
            raise YouTubeException(
                ErrorMessage(
                    status_code=ErrorCode.MISSING_PARAMS,
                    message="You must provide your credentials.",
                )
            )

        if method == "GET" and key not in args:
            args[key] = access_token

        try:
            response = self.session.request(
                method=method,
                url=self.BASE_URL + resource,
                timeout=self._timeout,
                params=args,
                proxies=self.proxies,
            )
        except requests.HTTPError as e:
            raise YouTubeException(
                ErrorMessage(status_code=ErrorCode.HTTP_ERROR, message=e.args[0])
            )
        else:
            return response

    def paged_by_page_token(
        self, resource: str, args: dict, count: Optional[int] = None,
    ):
        """
        Response paged by response's page token. If not provide response token
        Args:
            resource (str):
                The resource string need to retrieve data.
            args (dict)
                The args for api.
            count (int, optional):
                The count for result items you want to get.
                If provide this with None, will retrieve all items.
                Note:
                    The all items maybe too much. Notice your app's cost.
        Returns:
            Data api origin response.
        """
        res_data: Optional[dict] = None
        current_items: List[dict] = []
        page_token: Optional[str] = None
        now_items_count: int = 0

        while True:
            if page_token is not None:
                args["pageToken"] = page_token

            resp = self._request(resource=resource, method="GET", args=args)
            data = self._parse_response(resp)  # origin response
            # set page token
            page_token = data.get("nextPageToken")
            prev_page_token = data.get("prevPageToken")

            # parse results.
            items = self._parse_data(data)
            current_items.extend(items)
            now_items_count += len(items)
            if res_data is None:
                res_data = data
            # first check the count if satisfies.
            if count is not None:
                if now_items_count >= count:
                    current_items = current_items[:count]
                    break
            # if have no page token, mean no more data.
            if page_token is None:
                break
        res_data["items"] = current_items

        # use last request page token
        res_data["nextPageToken"] = page_token
        res_data["prevPageToken"] = prev_page_token
        return res_data

    def get_playlist_items(
        self,
        *,
        playlist_id: str,
        parts: Optional[Union[str, list, tuple, set]] = None,
        video_id: Optional[str] = None,
        count: Optional[int] = 5,
        limit: Optional[int] = 5,
        page_token: Optional[str] = None,
        return_json: Optional[bool] = True,
    ):
        """
        Retrieve playlist Items info by your given playlist id
        Args:
            playlist_id (str):
                The id for playlist that you want to retrieve items data.
            parts ((str,list,tuple,set) optional):
                The resource parts for you want to retrieve.
                If not provide, use default public parts.
                You can pass this with single part str, comma-separated parts str or a list,tuple,set of parts.
            video_id (str, Optional):
                Specifies that the request should return only the playlist items that contain the specified video.
            count (int, optional):
                The count will retrieve playlist items data.
                Default is 5.
                If provide this with None, will retrieve all playlist items.
            limit (int, optional):
                The maximum number of items each request retrieve.
                For playlistItem, this should not be more than 50.
                Default is 5
            page_token(str, optional):
                The token of the page of playlist items result to retrieve.
                You can use this retrieve point result page directly.
                And you should know about the the result set for YouTube.
            return_json(bool, optional):
                The return data type. If you set True JSON data will be returned.
                Now it's always True.
        Returns:
            original data
        """

        if count is None:
            limit = 50  # for playlistItems the max limit for per request is 50
        else:
            limit = min(count, limit)

        args = {
            "playlistId": playlist_id,
            "part": enf_parts(resource="playlistItems", value=parts),
            "maxResults": limit,
        }
        if video_id is not None:
            args["videoId"] = video_id

        if page_token is not None:
            args["pageToken"] = page_token

        res_data = self.paged_by_page_token(
            resource="playlistItems", args=args, count=count
        )
        if return_json:
            return res_data

    def get_channel_info(
        self,
        *,
        channel_id: Optional[Union[str, list, tuple, set]] = None,
        channel_name: Optional[str] = None,
        mine: Optional[bool] = None,
        parts: Optional[Union[str, list, tuple, set]] = None,
        hl: str = "en_US",
        return_json: Optional[bool] = True,
    ):
        """
        Retrieve channel data from YouTube Data API.
        Note:
            1. Don't know why, but now you could't get channel list by given an guide category.
               You can only get list by parameters mine,forUsername,id.
               Refer: https://developers.google.com/youtube/v3/guides/implementation/channels
            2. The origin maxResult param not work for these filter method.
        Args:
            channel_id ((str,list,tuple,set), optional):
                The id or comma-separated id string for youtube channel which you want to get.
                You can also pass this with an id list, tuple, set.
            channel_name (str, optional):
                The name for youtube channel which you want to get.
            mine (bool, optional):
                If you have give the authorization. Will return your channels.
                Must provide the access token.
            parts (str, optional):
                Comma-separated list of one or more channel resource properties.
                If not provided. will use default public properties.
            hl (str, optional):
                If provide this. Will return channel's language localized info.
                This value need https://developers.google.com/youtube/v3/docs/i18nLanguages.
            return_json(bool, optional):
                The return data type. If you set True JSON data will be returned.
        Returns:
            original data.
        """

        args = {
            "part": enf_parts(resource="channels", value=parts),
            "hl": hl,
        }
        if channel_name is not None:
            args["forUsername"] = channel_name
        elif channel_id is not None:
            args["id"] = enf_comma_separated("channel_id", channel_id)
        elif mine is not None:
            args["mine"] = mine
        else:
            raise YouTubeException(
                ErrorMessage(
                    status_code=ErrorCode.MISSING_PARAMS,
                    message=f"Specify at least one of channel_id,channel_name or mine",
                )
            )

        resp = self._request(resource="channels", method="GET", args=args)

        data = self._parse_response(resp)
        if return_json:
            return data

    def get_video_by_id(
        self,
        *,
        video_id: Union[str, list, tuple, set],
        parts: Optional[Union[str, list, tuple, set]] = None,
        hl: Optional[str] = "en_US",
        max_height: Optional[int] = None,
        max_width: Optional[int] = None,
        return_json: Optional[bool] = True,
    ):
        """
        Retrieve video data by given video id.
        Args:
            video_id ((str,list,tuple,set)):
                The id for video that you want to retrieve data.
                You can pass this with single id str, comma-separated id str, or a list,tuple,set of ids.
            parts ((str,list,tuple,set), optional):
                The resource parts for you want to retrieve.
                If not provide, use default public parts.
                You can pass this with single part str, comma-separated parts str or a list,tuple,set of parts.
            hl (str, optional):
                If provide this. Will return video's language localized info.
                This value need https://developers.google.com/youtube/v3/docs/i18nLanguages.
            max_height (int, optional):
                Specifies the maximum height of the embedded player returned in the player.embedHtml property.
                Acceptable values are 72 to 8192, inclusive.
            max_width (int, optional):
                Specifies the maximum width of the embedded player returned in the player.embedHtml property.
                Acceptable values are 72 to 8192, inclusive.
                If provide max_height at the same time. This will may be shorter than max_height.
                For more https://developers.google.com/youtube/v3/docs/videos/list#parameters.
            return_json(bool, optional):
                The return data type. If you set True JSON data will be returned.
        Returns:
            original data
        """

        args = {
            "id": enf_comma_separated(field="video_id", value=video_id),
            "part": enf_parts(resource="videos", value=parts),
            "hl": hl,
        }
        if max_height is not None:
            args["maxHeight"] = max_height
        if max_width is not None:
            args["maxWidth"] = max_width

        resp = self._request(resource="videos", method="GET", args=args)
        data = self._parse_response(resp)

        if return_json:
            return data

    def search(
        self,
        *,
        parts: Optional[Union[str, list, tuple, set]] = None,
        for_developer: Optional[bool] = None,
        for_mine: Optional[bool] = None,
        related_to_video_id: Optional[str] = None,
        channel_id: Optional[str] = None,
        channel_type: Optional[str] = None,
        event_type: Optional[str] = None,
        location: Optional[str] = None,
        location_radius: Optional[str] = None,
        count: Optional[int] = 50,
        limit: Optional[int] = 50,
        order: Optional[str] = None,
        published_after: Optional[str] = None,
        published_before: Optional[str] = None,
        q: Optional[str] = None,
        region_code: Optional[str] = None,
        relevance_language: Optional[str] = None,
        safe_search: Optional[str] = None,
        topic_id: Optional[str] = None,
        search_type: Optional[Union[str, list, tuple, set]] = None,
        video_caption: Optional[str] = None,
        video_category_id: Optional[str] = None,
        video_definition: Optional[str] = None,
        video_dimension: Optional[str] = None,
        video_duration: Optional[str] = None,
        video_embeddable: Optional[str] = None,
        video_license: Optional[str] = None,
        video_syndicated: Optional[str] = None,
        video_type: Optional[str] = None,
        page_token: Optional[str] = None,
        return_json: Optional[bool] = True,
    ):
        """
        Main search api implementation.
        You can find all parameters description at https://developers.google.com/youtube/v3/docs/search/list#parameters
        Returns:
            original data
        """
        parts = enf_parts(resource="search", value=parts)
        if search_type is None:
            search_type = "video,channel,playlist"
        else:
            search_type = enf_comma_separated(field="search_type", value=search_type)

        args = {
            "part": parts,
            "maxResults": min(limit, count),
        }
        if for_developer:
            args["forDeveloper"] = for_developer
        if for_mine:
            args["forMine"] = for_mine
        if related_to_video_id:
            args["relatedToVideoId"] = related_to_video_id
        if channel_id:
            args["channelId"] = channel_id
        if channel_type:
            args["channelType"] = channel_type
        if event_type:
            args["eventType"] = event_type
        if location:
            args["location"] = location
        if location_radius:
            args["locationRadius"] = location_radius
        if order:
            args["order"] = order
        if published_after:
            args["publishedAfter"] = published_after
        if published_before:
            args["publishedBefore"] = published_before
        if q:
            args["q"] = q
        if region_code:
            args["regionCode"] = region_code
        if relevance_language:
            args["relevanceLanguage"] = relevance_language
        if safe_search:
            args["safeSearch"] = safe_search
        if topic_id:
            args["topicId"] = topic_id
        if search_type:
            args["type"] = search_type
        if video_caption:
            args["videoCaption"] = video_caption
        if video_category_id:
            args["videoCategoryId"] = video_category_id
        if video_definition:
            args["videoDefinition"] = video_definition
        if video_dimension:
            args["videoDimension"] = video_dimension
        if video_duration:
            args["videoDuration"] = video_duration
        if video_embeddable:
            args["videoEmbeddable"] = video_embeddable
        if video_license:
            args["videoLicense"] = video_license
        if video_syndicated:
            args["videoSyndicated"] = video_syndicated
        if video_type:
            args["videoType"] = video_type
        if page_token:
            args["pageToken"] = page_token

        res_data = self.paged_by_page_token(resource="search", args=args, count=count)

        if return_json:
            return res_data

    def search_by_keywords(
        self,
        *,
        q: Optional[str],
        parts: Optional[Union[str, list, tuple, set]] = None,
        search_type: Optional[Union[str, list, tuple, set]] = None,
        count: Optional[int] = 25,
        limit: Optional[int] = 25,
        page_token: Optional[str] = None,
        return_json: Optional[bool] = True,
        **kwargs: Optional[dict],
    ):
        """
        This is simplest usage for search api. You can only passed the keywords to retrieve data from YouTube.
        And the result will include videos,playlists and channels.
        Note: A call to this method has a quota cost of 100 units.
        Args:
            q (str):
                Your keywords can also use the Boolean NOT (-) and OR (|) operators to exclude videos or
                to find videos that are associated with one of several search terms. For example,
                to search for videos matching either "boating" or "sailing",
                set the q parameter value to boating|sailing. Similarly,
                to search for videos matching either "boating" or "sailing" but not "fishing",
                set the q parameter value to boating|sailing -fishing.
                Note that the pipe character must be URL-escaped when it is sent in your API request.
                The URL-escaped value for the pipe character is %7C.
            parts ((str,list,tuple,set) optional):
                The resource parts for you want to retrieve.
                If not provide, use default public parts.
                You can pass this with single part str, comma-separated parts str or a list,tuple,set of parts.
            search_type ((str,list,tuple,set), optional):
                Parameter restricts a search query to only retrieve a particular type of resource.
                You can pass this with single part str, comma-separated parts str or a list,tuple,set of parts.
                The default value is video,channel,playlist
                Acceptable values are:
                    - channel
                    - playlist
                    - video
            count (int, optional):
                The count will retrieve videos data.
                Default is 25.
            limit (int, optional):
                The maximum number of items each request retrieve.
                For search, this should not be more than 50.
                Default is 25.
            page_token (str, optional):
                The token of the page of search result to retrieve.
                You can use this retrieve point result page directly.
                And you should know about the the result set for YouTube.
            return_json(bool, optional):
                The return data type. If you set True JSON data will be returned.
            kwargs:
                If you want use this pass more args. You can use this.
        Returns:
            original data
        """
        return self.search(
            parts=parts,
            q=q,
            search_type=search_type,
            count=count,
            limit=limit,
            page_token=page_token,
            return_json=return_json,
            **kwargs,
        )

    def search_by_developer(
        self,
        *,
        parts: Optional[Union[str, list, tuple, set]],
        q: Optional[str] = None,
        count: Optional[int] = 25,
        limit: Optional[int] = 25,
        page_token: Optional[str] = None,
        return_json: Optional[bool] = True,
        **kwargs,
    ):
        """
        Parameter restricts the search to only retrieve videos uploaded via the developer's application or website.
        Args:
            parts:
                The resource parts for you want to retrieve.
                If not provide, use default public parts.
                You can pass this with single part str, comma-separated parts str or a list,tuple,set of parts.
            q:
                Your keywords can also use the Boolean NOT (-) and OR (|) operators to exclude videos or
                to find videos that are associated with one of several search terms. For example,
                to search for videos matching either "boating" or "sailing",
                set the q parameter value to boating|sailing. Similarly,
                to search for videos matching either "boating" or "sailing" but not "fishing",
                set the q parameter value to boating|sailing -fishing.
                Note that the pipe character must be URL-escaped when it is sent in your API request.
                The URL-escaped value for the pipe character is %7C.
            count:
                The count will retrieve videos data.
                Default is 25.
            limit:
                The maximum number of items each request retrieve.
                For search, this should not be more than 50.
                Default is 25.
            page_token:
                The token of the page of search result to retrieve.
                You can use this retrieve point result page directly.
                And you should know about the the result set for YouTube.
            return_json:
                The return data type. If you set True JSON data will be returned.
            kwargs:
                If you want use this pass more args. You can use this.
        Returns:
            SearchListResponse or original data
        """
        return self.search(
            for_developer=True,
            search_type="video",
            parts=parts,
            q=q,
            count=count,
            limit=limit,
            page_token=page_token,
            return_json=return_json,
            **kwargs,
        )

    def search_by_mine(
        self,
        *,
        parts: Optional[Union[str, list, tuple, set]],
        q: Optional[str] = None,
        count: Optional[int] = 25,
        limit: Optional[int] = 25,
        page_token: Optional[str] = None,
        return_json: Optional[bool] = True,
        **kwargs,
    ):
        """
        Parameter restricts the search to only retrieve videos owned by the authenticated user.
        Note:
            This methods can not use following parameters:
            video_definition, video_dimension, video_duration, video_license,
            video_embeddable, video_syndicated, video_type.
        Args:
            q:
                Your keywords can also use the Boolean NOT (-) and OR (|) operators to exclude videos or
                to find videos that are associated with one of several search terms. For example,
                to search for videos matching either "boating" or "sailing",
                set the q parameter value to boating|sailing. Similarly,
                to search for videos matching either "boating" or "sailing" but not "fishing",
                set the q parameter value to boating|sailing -fishing.
                Note that the pipe character must be URL-escaped when it is sent in your API request.
                The URL-escaped value for the pipe character is %7C.
            parts ((str,list,tuple,set) optional):
                The resource parts for you want to retrieve.
                If not provide, use default public parts.
                You can pass this with single part str, comma-separated parts str or a list,tuple,set of parts.
            count (int, optional):
                The count will retrieve videos data.
                Default is 25.
            limit (int, optional):
                The maximum number of items each request retrieve.
                For search, this should not be more than 50.
                Default is 25.
            page_token (str, optional):
                The token of the page of search result to retrieve.
                You can use this retrieve point result page directly.
                And you should know about the the result set for YouTube.
            return_json(bool, optional):
                The return data type. If you set True JSON data will be returned.
            kwargs:
                If you want use this pass more args. You can use this.
        Returns:
            original data
        """
        return self.search(
            for_mine=True,
            search_type="video",
            parts=parts,
            q=q,
            count=count,
            limit=limit,
            page_token=page_token,
            return_json=return_json,
            **kwargs,
        )

    def search_by_related_video(
        self,
        *,
        related_to_video_id: str,
        parts: Optional[Union[str, list, tuple, set]] = None,
        region_code: Optional[str] = None,
        relevance_language: Optional[str] = None,
        safe_search: Optional[str] = None,
        count: Optional[int] = 25,
        limit: Optional[int] = 25,
        page_token: Optional[str] = None,
        return_json: Optional[bool] = True,
    ):
        """
        Retrieve a list of videos related to that video.
        Args:
            related_to_video_id:
                 A YouTube video ID which result associated with.
            parts:
                The resource parts for you want to retrieve.
                If not provide, use default public parts.
                You can pass this with single part str, comma-separated parts str or a list,tuple,set of parts.
            region_code:
                Parameter instructs the API to return search results for videos
                that can be viewed in the specified country.
            relevance_language:
                Parameter instructs the API to return search results that are most relevant to the specified language.
            safe_search:
                Parameter indicates whether the search results should include restricted content
                as well as standard content.
                Acceptable values are:
                    - moderate – YouTube will filter some content from search results and, at the least,
                      will filter content that is restricted in your locale. Based on their content,
                      search results could be removed from search results or demoted in search results.
                      This is the default parameter value.
                    - none – YouTube will not filter the search result set.
                    - strict – YouTube will try to exclude all restricted content from the search result set.
                      Based on their content, search results could be removed from search results or
                      demoted in search results.
            count:
                The count will retrieve videos data.
                Default is 25.
            limit:
                The maximum number of items each request retrieve.
                For search, this should not be more than 50.
                Default is 25.
            page_token:
                The token of the page of search result to retrieve.
                You can use this retrieve point result page directly.
                And you should know about the the result set for YouTube.
            return_json:
                The return data type. If you set True JSON data will be returned.
        Returns:
            If you want use this pass more args. You can use this.
        """

        return self.search(
            parts=parts,
            related_to_video_id=related_to_video_id,
            search_type="video",
            region_code=region_code,
            relevance_language=relevance_language,
            safe_search=safe_search,
            count=count,
            limit=limit,
            page_token=page_token,
            return_json=return_json,
        )
