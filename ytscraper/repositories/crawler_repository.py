import json
import logging

from ytscraper.apps.crawlers.models import YTVideo


logger = logging.getLogger(__name__)


class YTVideoRepository:

    @staticmethod
    def save_videos_by_items(channel, items):
        for vitem in items:
            snippet = vitem.get("snippet", {})
            tags = ", ".join(snippet.get("tags", []))
            ch_dict = {
                "id": vitem.get("id", ""),
                "title": snippet.get("title", ""),
                "description": snippet.get("description", ""),
                "thumb_url": snippet.get("thumbnails", {})
                .get("high", {})
                .get("url", ""),
                "metadata": json.dumps(vitem),
                "statistics": json.dumps(vitem.get("statistics", {})),
                "view_count": vitem.get("statistics", {}).get("viewCount", 0),
                "tags": tags,
                "channel": channel,
            }
            try:
                YTVideo.objects.get(id=vitem.get("id"))
            except YTVideo.DoesNotExist as de:
                logger.error(de)
                YTVideo.objects.create(**ch_dict)
