from ytscraper.apps.crawlers.models import YTChannel, YTVideo
from ytscraper.apps.crawlers.tests.factories import YTChannelFactory, YTVideoFactory


class TestYTChannelModel:
    def test_model(self, channel):
        exiting = YTChannel.objects.last()
        assert exiting.id == channel.id


class TestYTVideoModel:
    def test_model(self, video):
        exiting = YTVideo.objects.last()
        assert exiting.id == video.id
