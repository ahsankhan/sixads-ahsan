from factory import DjangoModelFactory, SubFactory, Faker
from factory.fuzzy import FuzzyText

from ytscraper.apps.crawlers.models import YTChannel, YTVideo


class YTChannelFactory(DjangoModelFactory):
    id = FuzzyText(length=8)
    title = Faker("name")
    description = Faker("sentence")

    class Meta:
        model = YTChannel


class YTVideoFactory(DjangoModelFactory):
    id = FuzzyText(length=8)
    title = Faker("name")
    channel = SubFactory(YTChannelFactory)
    description = Faker("sentence")
    tags = Faker("word")

    class Meta:
        model = YTVideo
