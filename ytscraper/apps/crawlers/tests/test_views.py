from pytest_django.asserts import assertTemplateUsed, assertTemplateNotUsed

from rest_framework.reverse import reverse


class TestIndexView:
    url = reverse("crawlers:index")

    def test_should_user_incorrect_template_to_render(self, client):
        response = client.get(self.url)

        assertTemplateNotUsed(response, "index.html")

    def test_should_user_correct_template_to_render(self, client):
        response = client.get(self.url)

        assert response.status_code == 200
        assertTemplateUsed(response, "crawlers/index.html")


class TestVideoListAPIView:
    url = reverse("crawlers:video-list")

    def test_video_list_api_failed(self, client):
        response = client.get(self.url)

        assert len(response.data["results"]) == 0

    def test_video_list_api_by_tag_failed(self, client):
        url = self.url + "?tag={}".format("random")
        response = client.get(url)

        assert len(response.data["results"]) == 0

    def test_video_list_api_by_performance_failed(self, client):
        url = self.url + "?performance=1"
        response = client.get(url)

        assert len(response.data["results"]) == 0

    def test_video_list_api(self, client, video):
        response = client.get(self.url)

        assert response.status_code == 200
        assert len(response.data["results"]) > 0

    def test_video_list_api_by_tag(self, client, video):
        url = self.url + "?tag={}".format(video.tags)
        response = client.get(url)

        assert response.status_code == 200
        assert len(response.data["results"]) > 0

    def test_video_list_api_by_performance(self, client, video):
        url = self.url + "?performance=1"
        response = client.get(url)

        assert response.status_code == 200
        assert len(response.data["results"]) > 0
