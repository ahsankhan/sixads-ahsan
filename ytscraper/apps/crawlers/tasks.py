from ytscraper.repositories.crawler_repository import YTVideoRepository
from ytscraper.apps.crawlers.constants import VIDEO_UPDATE_DURATION_HOUR
from celery.utils.log import get_task_logger
from celery.schedules import crontab

from django.conf import settings

import json

from ytscraper.apps.crawlers.models import YTChannel, YTVideo
from ytscraper.errors import YouTubeException
from ytscraper.services.youtube import YTScraperService

from ytscraper.celery import app


logger = get_task_logger(__name__)


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    # Executes every 2 hours
    sender.add_periodic_task(
        crontab(hour=f"*/{VIDEO_UPDATE_DURATION_HOUR}", minute="0"),
        preodic_youtube_video_crawl.s(),
    )


@app.task
def initiate_youtube_crawl():
    try:
        crawler = YTScraperService(api_key=settings.YOUTUBE_API_KEY)
        channels = crawler.get_channel_info(channel_id=settings.CHANNEL_ID)
        items = channels.get("items", [])
        for ch_item in items:
            channel_id = ch_item.get("id", "")
            ch_dict = {
                "id": channel_id,
                "title": ch_item.get("snippet", {}).get("title", ""),
                "description": ch_item.get("snippet", {}).get("description", ""),
                "thumb_url": ch_item.get("snippet", {})
                .get("thumbnails", {})
                .get("default", {})
                .get("url", ""),
                "metadata": json.dumps(ch_item),
                "video_count": ch_item.get("statistics", {}).get("videoCount", 0),
            }
            try:
                ch = YTChannel.objects.get(id=channel_id)
                if ch.video_count < int(ch_dict.get("video_count")):
                    youtube_video_crawl.delay(channel_id)
            except YTChannel.DoesNotExist as de:
                logger.error(de)
                YTChannel.objects.create(**ch_dict)
                youtube_video_crawl.delay(channel_id)
    except YouTubeException as e:
        logger.error(e)
    except Exception as exc:
        logger.error(exc)


@app.task
def youtube_video_crawl(channel_id, page_token=None):
    try:
        channel = YTChannel.objects.get(id=channel_id)
        crawler = YTScraperService(api_key=settings.YOUTUBE_API_KEY)
        results = crawler.search(
            channel_id=channel_id,
            order="date",
            search_type="video",
            page_token=page_token,
            parts="id",
        )
        items = crawler._parse_data(results)
        videoIds = [
            item.get("id", {}).get("videoId")
            for item in items
            if "video" in item.get("id", {}).get("kind")
        ]
        videos = crawler.get_video_by_id(video_id=videoIds)
        vitems = crawler._parse_data(videos)
        YTVideoRepository.save_videos_by_items(channel, vitems)
        next_token = results.get("nextPageToken", "")
        if next_token:
            youtube_video_crawl.delay(channel_id, page_token=next_token)

        playListIds = [
            item.get("id", {}).get("playlistId")
            for item in items
            if "playlist" in item.get("id", {}).get("kind")
        ]
        for playlist in playListIds:
            youtube_video_crawl_by_playlistId.delay(playlist, page_token=None)
    except YouTubeException as e:
        logger.error(e)
    except (YTChannel.DoesNotExist, Exception) as exc:
        logger.error(exc)


@app.task
def youtube_video_crawl_by_playlistId(playlist_id, page_token=None):
    try:
        
        crawler = YTScraperService(api_key=settings.YOUTUBE_API_KEY)
        results = crawler.get_playlist_items(
            playlist_id=playlist_id,
            page_token=page_token,
            parts="snippet",
        )

        items = crawler._parse_data(results)

        if items:
            pl_snippet = items[0].get("snippet", {})
            channel = YTChannel.objects.get(id=pl_snippet.get("channelId", ""))

            videoIds = [
                item.get("snippet", {}).get("resourceId", {}).get("videoId")
                for item in items
                if item.get("snippet", {}).get("resourceId", {}).get("videoId")
            ]
            videos = crawler.get_video_by_id(video_id=videoIds)
            vitems = crawler._parse_data(videos)
            YTVideoRepository.save_videos_by_items(channel, vitems)
            next_token = results.get("nextPageToken", "")
            if next_token:
                youtube_video_crawl_by_playlistId.delay(playlist_id, page_token=next_token)
    except YouTubeException as e:
        logger.error(e)
    except (YTChannel.DoesNotExist, Exception) as exc:
        logger.error(exc)


@app.task
def preodic_youtube_video_crawl():
    videos = YTVideo.objects.all()
    try:
        crawler = YTScraperService(api_key=settings.YOUTUBE_API_KEY)
        for video in videos:
            video_data = crawler.get_video_by_id(video_id=video.id, parts="statistics")
            vitems = crawler._parse_data(video_data)

            if vitems:
                vitem = vitems[0]

                video.statistics = json.dumps(vitem.get("statistics", {}))
                video.view_count = vitem.get("statistics", {}).get("viewCount", 0)
                video.save()
    except YouTubeException as yte:
        logger.error(yte)
    except Exception as e:
        logger.error(e)
