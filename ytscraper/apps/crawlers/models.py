import json
import logging
from json import JSONDecodeError

from django.db import models


logger = logging.getLogger(__name__)


class BaseModel(models.Model):
    id = models.CharField(max_length=255, primary_key=True)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, default="")
    thumb_url = models.URLField(null=True)
    metadata = models.JSONField(blank=True, null=True)
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name="%(class)s_created_at"
    )
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name="%(class)s_updated_at"
    )

    def get_json_data(self, json_str):
        try:
            return json.loads(json_str)
        except (TypeError, JSONDecodeError) as error:
            logger.debug(error)
            return {}

    def get_metadata_dict(self) -> dict:
        if self.metadata:
            return self.get_json_data(self.metadata)
        return {}

    class Meta:
        abstract = True


class YTChannel(BaseModel):
    visited = models.BooleanField(default=False)
    video_count = models.PositiveBigIntegerField(default=0)

    class Meta:
        verbose_name = "Youtube Channel"


class YTVideo(BaseModel):
    channel = models.ForeignKey(
        "crawlers.YTChannel", related_name="channel_videos", on_delete=models.CASCADE
    )
    tags = models.TextField(null=True, blank=True)
    statistics = models.JSONField(null=True, blank=True)
    view_count = models.PositiveBigIntegerField(default=0, db_index=True)

    def get_statistics_dict(self) -> dict:
        if self.statistics:
            return self.get_json_data(self.statistics)
        return {}

    class Meta:
        verbose_name = "Youtube Video"
