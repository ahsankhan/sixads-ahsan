import logging

from rest_framework import serializers
from ytscraper.apps.crawlers.models import YTVideo


logger = logging.getLogger(__name__)


class YTVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = YTVideo
        exclude = ["metadata", "statistics"]
