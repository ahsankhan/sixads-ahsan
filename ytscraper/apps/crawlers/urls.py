from django.urls import path

from .views import IndexView, VideoListAPIView

app_name = "crawlers"

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("videos/", VideoListAPIView.as_view(), name="video-list"),
]
