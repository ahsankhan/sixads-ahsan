import time
from django.core.management import BaseCommand

from ytscraper.apps.crawlers.tasks import initiate_youtube_crawl


class Command(BaseCommand):
    help = "Initiate crawling task."

    def handle(self, *args, **options):
        time.sleep(2)
        initiate_youtube_crawl.delay()
