var showMoreClick = (e) => {
    e.preventDefault();
    const elm = e.target;
    let url = $(elm).attr("href");
    console.log(url);
    getVideos(url);
}

var getVideos = (url) => {
    $.get(url, function(data, status){
        $.each(data.results, function(index, item){
            let dataStr = `<div class="col mb-5">
                <div class="card h-100">
                    <!-- Product image-->
                    <img class="card-img-top" src="${item.thumb_url}" alt="${item.title}" />
                    <!-- Product details-->
                    <div class="card-body p-4">
                        <div class="text-center">
                            <!-- Product name-->
                            <h5 class="fw-bolder">${item.title}</h5>
                        </div>
                    </div>
                    <!-- Product actions-->
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="https://youtube.com/watch?v=${item.id}" target="_blank">View</a></div>
                    </div>
                </div>
            </div>`;

            $("#contentLoaderDiv").append(dataStr);
        });
        
        if(data.next !== null && data.next !== undefined){
            $("#showMore").attr("href", data.next);
        } else {
            $("#showMoreDiv").html('');
        }
            
    });
}

$(function() {
    getVideos("http://localhost:8000/videos/");

    // Tag form submit
    $("#tagForm").submit(function(e){
        e.preventDefault();
        let url = encodeURI(`http://localhost:8000/videos/?tag=${$("#inputTag").val()}`);
        $("#contentLoaderDiv").html('');
        $("#inputTag").val("");
        getVideos(url);
    });

    // Performance form submit
    $("#performanceForm").submit(function(e){
        e.preventDefault();
        let url = encodeURI(`http://localhost:8000/videos/?performance=1`);
        $("#contentLoaderDiv").html('');
        getVideos(url);
    })
});