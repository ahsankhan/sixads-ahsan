from ytscraper.apps.crawlers.models import YTVideo
from django.contrib import admin

from ytscraper.apps.crawlers.models import YTChannel, YTVideo

admin.site.register(YTChannel)
admin.site.register(YTVideo)
