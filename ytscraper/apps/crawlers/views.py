from urllib.parse import unquote

from django.contrib.postgres.search import SearchQuery, SearchVector
from django.db.models import F, FloatField
from django.db.models.functions import Cast
from django.shortcuts import render
from django.views import generic

from rest_framework import generics

from ytscraper.apps.crawlers.models import YTVideo
from ytscraper.apps.crawlers.serializers import YTVideoSerializer
from ytscraper.utils.postgres.aggregate import Median


class IndexView(generic.ListView):
    template_name = "crawlers/index.html"
    context_object_name = "video_list"

    def get_queryset(self):
        """Return the last five published questions."""
        return YTVideo.objects.order_by("-updated_at")[:50]


class VideoListAPIView(generics.ListAPIView):
    serializer_class = YTVideoSerializer

    def get_queryset(self):
        queryset = YTVideo.objects.all()
        tag = self.request.query_params.get("tag")
        performance = self.request.query_params.get("performance")

        if tag and not performance:
            queryset = queryset.annotate(search=SearchVector("tags")).filter(
                search=SearchQuery(unquote(tag))
            )
        if performance and not tag:
            median = YTVideo.objects.aggregate(median=Median("view_count")).get(
                "median"
            )
            if median is None or median <= 0:
                median = 1
            queryset = queryset.annotate(
                calculated_views=Cast(
                    F("view_count") / median, output_field=FloatField()
                )
            ).order_by("-calculated_views")

        return queryset
