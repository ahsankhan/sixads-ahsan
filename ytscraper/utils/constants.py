"""
    some constants for YouTube
"""

CHANNEL_RESOURCE_PROPERTIES = {
    "id",
    "snippet",
    "statistics",
    "status",
    "topicDetails",
}

PLAYLIST_ITEM_RESOURCE_PROPERTIES = {"id", "contentDetails", "snippet", "status"}

VIDEO_RESOURCE_PROPERTIES = {
    "id",
    "snippet",
    "statistics",
}

SEARCH_RESOURCE_PROPERTIES = {"id", "snippet"}

RESOURCE_PARTS_MAPPING = {
    "channels": CHANNEL_RESOURCE_PROPERTIES,
    "videos": VIDEO_RESOURCE_PROPERTIES,
    "search": SEARCH_RESOURCE_PROPERTIES,
    "playlistItems": PLAYLIST_ITEM_RESOURCE_PROPERTIES,
}
